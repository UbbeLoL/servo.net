﻿using System;
using System.Collections.Generic;
using Mono.Cecil;
using Servo.Core.Context;
using Servo.Core.Protections;
using Servo.Core.Protections.Base;

namespace Servo.Core
{
    public class Protector
    {
        private readonly List<IProtection> _protections = new List<IProtection>
                                                    {
                                                        Activator.CreateInstance<Analyze>(),
                                                        Activator.CreateInstance<Preparation>(),
                                                        //Activator.CreateInstance<ResourceProtection>(),
                                                        Activator.CreateInstance<StringProtection>()
                                                    };

        private readonly List<IPostProtection> _postProtections = new List<IPostProtection>();

        public void ProcessFile(string input)
        {
            Protect(AssemblyDefinition.ReadAssembly(input), input);
        }

        private void Protect(AssemblyDefinition asmDef, string input)
        {
            Session.ActiveContext = new StandardProtectorContext(asmDef, input, input + "_obf.exe")
                                        {
                                            OutputDisplay = new ConsoleOutputDisplay(OutputLevel.Verbose),
                                            ProtectionLevel = ProtectionLevel.Complete
                                        };
            
            _protections.ForEach(x =>
                                     {
                                         if ((int) x.RequiredLevel > (int) Session.ActiveContext.ProtectionLevel)
                                             return;

                                         x.Initialize(Session.ActiveContext);
                                         x.Process();
                                     });

            AssemblyWorker.SaveAssembly();

            _postProtections.ForEach(x =>
                                     {
                                         if ((int) x.RequiredLevel > (int) Session.ActiveContext.ProtectionLevel)
                                             return;

                                         x.Initialize(Session.ActiveContext);
                                         x.Process();
                                     });
        }
    }
}
