﻿namespace Servo.Core
{
    public static class AssemblyWorker
    {
        public static void SaveAssembly()
        {
            Session.ActiveContext.LoadedAssembly.Write(Session.ActiveContext.DestinationAssembly);
        }
    }
}
