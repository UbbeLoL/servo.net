﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Mono.Cecil;
using Servo.Core.Analyzers;
using Servo.Core.Context;
using Servo.Core.Crypto;

namespace Servo.Core.Utilities
{
    public class NativeUtils
    {
        [DllImport(@"C:\Users\Mattias\Documents\Visual Studio 2012\Projects\Servo\Servo.Core\Debug\Servo.Security.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern void WriteToResource(
            string assemblyName,
            [MarshalAsAttribute(UnmanagedType.LPWStr)] string resName,
            IntPtr buffer,
            int size);

        public IntPtr MarshalResourceTable(IProtectorContext context, out int size)
        {
            var resTbl = context.AnalysisResults["Resources"] as ResourceAnalysisResult;
            var finalBuff = new List<byte>();
            var offset = resTbl.RawResources.Count*12;

            foreach (var res in resTbl.RawResources)
            {
                  /* Header:
                 * id (DWORD)
                 * size (DWORD)
                 * ptr to buffer (DWORD)
                 */

                var resData = ((EmbeddedResource) res.Item1).GetResourceData();

                finalBuff.AddRange(BitConverter.GetBytes(res.Item2));
                finalBuff.AddRange(BitConverter.GetBytes(resData.Length));
            }

            foreach (var res in resTbl.RawResources)
                finalBuff.AddRange(((EmbeddedResource) res.Item1).GetResourceData());

            var mem = Marshal.AllocHGlobal(finalBuff.Count);
            Marshal.Copy(finalBuff.ToArray(), 0, mem, finalBuff.Count);

            size = finalBuff.Count;

            return mem;
        }

        public IntPtr MarshalStringTable(IProtectorContext context, out int size)
        {
            var strTbl = context.AnalysisResults["Strings"] as StringAnalysisResult;
            var finalBuff = new List<byte>();
            var offset = strTbl.RawStrings.Count*16;

            foreach (var @string in strTbl.RawStrings)
            {
                var strBuff = new StringBuilder();
                strBuff.Append(@string.SourceInstruction.Operand as string);

                /* Header:
                 * mdTok (DWORD)
                 * id (DWORD)
                 * size (DWORD) (*2 for unicode and +2 for null terminator)
                 * ptr to buffer (DWORD)
                 */

                finalBuff.AddRange(BitConverter.GetBytes(@string.SourceMethod.MetadataToken.ToInt32()));
                finalBuff.AddRange(BitConverter.GetBytes(@string.ID));
                finalBuff.AddRange(BitConverter.GetBytes(strBuff.Length*2));
                finalBuff.AddRange(BitConverter.GetBytes(offset));
                offset += strBuff.Length*2;
            }

            foreach (var @string in strTbl.RawStrings)
            {
                var key = new byte[16];
                var iv = new byte[16];
                var rawString = @string.SourceInstruction.Operand as string;

                // Create Key
                Buffer.BlockCopy(BitConverter.GetBytes((@string.SourceMethod.MetadataToken.ToInt32() + @string.ID)), 0, key, 0, 4);
                Buffer.BlockCopy(BitConverter.GetBytes(-(@string.SourceMethod.MetadataToken.ToInt32() + @string.ID)), 0, key, 4, 4);
                Buffer.BlockCopy(BitConverter.GetBytes((@string.SourceMethod.MetadataToken.ToInt32() + @string.ID)), 0, key, 8, 4);
                Buffer.BlockCopy(BitConverter.GetBytes(-(@string.SourceMethod.MetadataToken.ToInt32() + @string.ID)), 0, key, 12, 4);

                // Create IV
                Buffer.BlockCopy(BitConverter.GetBytes((@string.SourceMethod.MetadataToken.ToInt32() - @string.ID)), 0, iv, 0, 4);
                Buffer.BlockCopy(BitConverter.GetBytes(-(@string.SourceMethod.MetadataToken.ToInt32() - @string.ID)), 0, iv, 4, 4);
                Buffer.BlockCopy(BitConverter.GetBytes((@string.SourceMethod.MetadataToken.ToInt32() - @string.ID)), 0, iv, 8, 4);
                Buffer.BlockCopy(BitConverter.GetBytes(-(@string.SourceMethod.MetadataToken.ToInt32() - @string.ID)), 0, iv, 12, 4);

                var hc = new HC128(key, iv);

                var encString = hc.EncryptDecrypt(Encoding.Unicode.GetBytes(rawString));
                finalBuff.AddRange(encString);
            }
            var mem = Marshal.AllocHGlobal(finalBuff.Count);
            Marshal.Copy(finalBuff.ToArray(), 0, mem, finalBuff.Count);

            size = finalBuff.Count;

            return mem;
        }
    }
}
