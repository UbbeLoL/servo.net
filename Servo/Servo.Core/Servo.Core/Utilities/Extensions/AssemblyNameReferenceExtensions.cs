﻿using System.IO;
using Mono.Cecil;

namespace Servo.Core.Utilities.Extensions
{
    public static class AssemblyNameReferenceExtensions
    {
        public static bool IsCore(this AssemblyNameReference @ref)
        {
            switch (@ref.Name)
            {
                case "mscorlib":
                case "Accessibility":
                case "Mono.Security":
                    return true;
                default:
                    return @ref.Name.StartsWith("System")
                        || @ref.Name.StartsWith("Microsoft");
            }
        }

        public static AssemblyDefinition ResolveReference(this AssemblyNameReference @ref)
        {
            var fixedPath = Path.Combine(Path.GetDirectoryName(Session.ActiveContext.SourceAssembly), @ref.Name + ".dll");
            return AssemblyDefinition.ReadAssembly(fixedPath);
        }

        public static AssemblyDefinition ResolveReference(this AssemblyNameReference @ref, out string path)
        {
            path = Path.Combine(Path.GetDirectoryName(Session.ActiveContext.SourceAssembly), @ref.Name + ".dll");
            return AssemblyDefinition.ReadAssembly(path);
        }
    }
}
