﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mono.Cecil;

namespace Servo.Core.Utilities.Extensions
{
    public static class AssemblyDefinitionExtensions
    {
        public static MethodDefinition FindMethod(this AssemblyDefinition asmDef, Predicate<MethodDefinition> pred)
        {
           // return (from modDef in asmDef.Modules from typeDef in modDef.Types from mDef in typeDef.Methods where mDef.HasBody select mDef).FirstOrDefault(mDef => pred(mDef));

            foreach (var tDef in asmDef.MainModule.Types)
            {
                foreach (var mDef in from nt in tDef.NestedTypes from mDef in nt.Methods where pred(mDef) select mDef)
                    return mDef;

                foreach (var mDef in tDef.Methods.Where(mDef => pred(mDef)))
                    return mDef;
            }

            return null;
        }

        public static IEnumerable<MethodDefinition> FindMethods(this AssemblyDefinition asmDef, Predicate<MethodDefinition> pred)
        {
            foreach (var tDef in asmDef.MainModule.Types)
            {
                foreach (var mDef in from nt in tDef.NestedTypes from mDef in nt.Methods where pred(mDef) select mDef)
                    yield return mDef;

                foreach (var mDef in tDef.Methods.Where(mDef => pred(mDef)))
                    yield return mDef;
            }
        }

        public static IEnumerable<TypeDefinition> FindTypes(this AssemblyDefinition asmDef, Predicate<TypeDefinition> pred)
        {
            return asmDef.MainModule.Types.Where(t => pred(t));
        }
    }
}
