﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mono.Cecil;
using Servo.Core.Context;
using Servo.Core.Protections.Base;
using Servo.Core.Utilities;

namespace Servo.Core.Protections
{
    class Preparation : IProtection
    {
        public IProtectorContext Context { get; set; }
        public string Description { get; private set; }
        public ProtectionLevel RequiredLevel { get; private set; }
        private ModuleDefinition _loadedModule;

        public void Initialize(IProtectorContext context)
        {
            _loadedModule = Session.ActiveContext.LoadedAssembly.MainModule;
        }

        public void Process()
        {
            var initializor = CecilHelper.Inject(_loadedModule,
                                      Session.ActiveContext.Injections.MainModule.GetType("Servo.Injections.Initializor"));
            _loadedModule.Types.Add(initializor);
        }
    }
}
