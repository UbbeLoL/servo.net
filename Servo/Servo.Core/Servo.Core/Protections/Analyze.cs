﻿using System;
using System.Collections.Generic;
using Servo.Core.Analyzers;
using Servo.Core.Analyzers.Base;
using Servo.Core.Context;
using Servo.Core.Protections.Base;

namespace Servo.Core.Protections
{
    public class Analyze : IProtection
    {
        public IProtectorContext Context { get; set; }
        public string Description { get { return "Analyze assembly"; } }
        public ProtectionLevel RequiredLevel { get { return ProtectionLevel.Basic; } }

        private readonly List<IAnalyzer> _analyzers = new List<IAnalyzer>
                                                 {
                                                     Activator.CreateInstance<SubsystemAnalyzer>(),
                                                     Activator.CreateInstance<ReferenceAnalyzer>(),
                                                     Activator.CreateInstance<StringAnalyzer>(),
                                                     Activator.CreateInstance<ResourceAnalyzer>()
                                                 };

        public void Initialize(IProtectorContext context)
        {
            context.AnalysisResults = new Dictionary<string, IAnalysisResult>();
        }

        public void Process()
        {
            _analyzers.ForEach(x => x.Analyze(Session.ActiveContext));
        }
    }
}
