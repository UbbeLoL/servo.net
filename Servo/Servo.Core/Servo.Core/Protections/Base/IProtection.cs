﻿using Servo.Core.Context;

namespace Servo.Core.Protections.Base
{
    public enum ProtectionLevel : byte
    {
        Basic = 0x0,
        Intermediate = 0x1,
        Complete = 0x2
    }

    interface IProtection : IContextDependent
    {
        string Description { get; }
        ProtectionLevel RequiredLevel { get; }

        void Process();
    }
}
