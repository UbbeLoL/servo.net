﻿using System;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Servo.Core.Analyzers;
using Servo.Core.Context;
using Servo.Core.Protections.Base;

namespace Servo.Core.Protections
{
    public class NativePreparation : IMemberProtection
    {
        public IProtectorContext Context { get; set; }
        public string Description { get { return "Prepare"; } }
        public ProtectionLevel RequiredLevel { get { return ProtectionLevel.Basic; } }

        public void Initialize(IProtectorContext context)
        {

        }

        public void Process()
        {
            var host = InjectHostType();
            var loadLibrary = CreateLoadCall();
            var decryptString = CreateStringDecryptCall();

            host.Methods.Add(loadLibrary);
            host.Methods.Add(decryptString);

            var ilProc = Session.ActiveContext.LoadedAssembly.EntryPoint.Body.GetILProcessor();
            var subSystem = Session.ActiveContext.AnalysisResults["Subsystem"] as SubsystemAnalysisResult;

            if (subSystem != null)
                switch (subSystem.Subsystem)
                {
                    case ModuleKind.Windows:
                        ilProc.Remove(ilProc.Body.Instructions.Count - 1);
                        ilProc.Emit(OpCodes.Ldstr, "Servo.Runtime.dll");
                        ilProc.Emit(OpCodes.Call, loadLibrary);
                        ilProc.Emit(OpCodes.Pop);
                        ilProc.Emit(OpCodes.Ret);
                        break;

                    case ModuleKind.Console:
                        var initial = ilProc.Create(OpCodes.Ldstr, "Servo.Runtime.dll");
                        ilProc.InsertBefore(ilProc.Body.Instructions[0], initial);
                        ilProc.InsertAfter(initial, ilProc.Create(OpCodes.Call, loadLibrary));
                        ilProc.InsertAfter(initial.Next, ilProc.Create(OpCodes.Pop));
                        break;
                }
        }

        private static TypeDefinition InjectHostType()
        {
            var host = new TypeDefinition("Servo", "Host", TypeAttributes.Public);
            Session.ActiveContext.LoadedAssembly.MainModule.Types.Add(host);

            return host;
        }

        private MethodDefinition CreateLoadCall()
        {
            var modRef = new ModuleReference("kernel32");
            Session.ActiveContext.LoadedAssembly.MainModule.ModuleReferences.Add(modRef);

            var loadLibrary = new MethodDefinition("LoadLibrary", MethodAttributes.Public | MethodAttributes.Static | MethodAttributes.PInvokeImpl | MethodAttributes.HideBySig,
                                                   Session.ActiveContext.LoadedAssembly.MainModule.Import(typeof(IntPtr))) { IsPInvokeImpl = true };

            loadLibrary.Parameters.Add(new ParameterDefinition("lpFileName", ParameterAttributes.None,
                                                               Session.ActiveContext.LoadedAssembly.MainModule.Import(typeof(String))));

            loadLibrary.PInvokeInfo = new PInvokeInfo(PInvokeAttributes.CallConvWinapi | PInvokeAttributes.CharSetUnicode | PInvokeAttributes.SupportsLastError, "LoadLibrary", modRef);
            loadLibrary.IsPreserveSig = true;

            return loadLibrary;
        }

        private MethodDefinition CreateStringDecryptCall()
        {
            var modRef = new ModuleReference("Servo.Runtime.dll");
            Session.ActiveContext.LoadedAssembly.MainModule.ModuleReferences.Add(modRef);

            var decryptString = new MethodDefinition("DecryptString", MethodAttributes.Public | MethodAttributes.Static | MethodAttributes.PInvokeImpl | MethodAttributes.HideBySig,
                                                   Session.ActiveContext.LoadedAssembly.MainModule.Import(typeof(string))) { IsPInvokeImpl = true };

            decryptString.Parameters.Add(new ParameterDefinition("nId", ParameterAttributes.None,
                                                               Session.ActiveContext.LoadedAssembly.MainModule.Import(typeof(int))));

            decryptString.PInvokeInfo = new PInvokeInfo(PInvokeAttributes.CallConvCdecl | PInvokeAttributes.CharSetUnicode | PInvokeAttributes.SupportsLastError, "DecryptString", modRef);
            decryptString.IsPreserveSig = true;

            return decryptString;
        }
    }
}
