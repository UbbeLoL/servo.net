﻿using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Servo.Core.Analyzers;
using Servo.Core.Context;
using Servo.Core.Protections.Base;
using Servo.Core.Utilities;
using Servo.Core.Utilities.Extensions;

namespace Servo.Core.Protections
{
    class StringProtection : IMemberProtection
    {
        public IProtectorContext Context { get; set; }
        public string Description { get { return "Strings"; } }
        public ProtectionLevel RequiredLevel { get { return ProtectionLevel.Intermediate; } }

        private readonly NativeUtils _utils = new NativeUtils();
        private ModuleDefinition _loadedModule;

        public void Initialize(IProtectorContext context)
        {
            _loadedModule = Session.ActiveContext.LoadedAssembly.MainModule;
        }

        public void Process()
        {
            var rawStringTable = Session.ActiveContext.AnalysisResults["Strings"] as StringAnalysisResult;
            var stringWorker = CecilHelper.Inject(_loadedModule,
                                                  Session.ActiveContext.Injections.MainModule.GetType("Servo.Injections.StringWorker"));
            _loadedModule.Types.Add(stringWorker);
            var getToken = stringWorker.Module.Assembly.FindMethod(x => x.Name == "GetToken");
            var decryptString = stringWorker.Module.Assembly.FindMethod(x => x.Name == "FetchString");


            var asmName = "";


           // File.WriteAllBytes(
            asmName = Path.GetDirectoryName(Session.ActiveContext.DestinationAssembly) + "\\Servo.Runtime.dll";
              //  Properties.Resources.Servo_Runtime);

            var buffSize = 0;
            var unmanagedStrTbl = _utils.MarshalStringTable(Session.ActiveContext, out buffSize);

            if (rawStringTable != null)
                rawStringTable.RawStrings.ForEach(x =>
                                                      {
                                                          var ilProc = x.SourceMethod.Body.GetILProcessor();

                                                          ilProc.InsertBefore(x.SourceInstruction,
                                                                              ilProc.Create(OpCodes.Ldc_I4, x.ID));
                                                          //ilProc.InsertBefore(x.SourceInstruction,
                                                                              //ilProc.Create(OpCodes.Call, getToken));
                                                      });

            NativeUtils.WriteToResource(asmName, "str", unmanagedStrTbl, buffSize);
            Marshal.FreeHGlobal(unmanagedStrTbl);

            if (rawStringTable != null)
                rawStringTable.RawStrings.ForEach(x =>
                                                      {
                                                          var ilProc = x.SourceMethod.Body.GetILProcessor();

                                                          ilProc.Replace(x.SourceInstruction,
                                                                         ilProc.Create(OpCodes.Call, decryptString));
                                                      });
        }
    }
}
