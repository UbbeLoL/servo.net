﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Servo.Core.Analyzers;
using Servo.Core.Context;
using Servo.Core.Protections.Base;
using Servo.Core.Utilities;

namespace Servo.Core.Protections
{
    class ResourceProtection : IMemberProtection
    {
        public IProtectorContext Context { get; set; }
        public string Description { get; private set; }
        public ProtectionLevel RequiredLevel { get; private set; }
        private readonly NativeUtils _utils = new NativeUtils();

        public void Initialize(IProtectorContext context)
        {
         
        }

        public void Process()
        {
            //var mngdResTbl = ((ResourceAnalysisResult) Session.ActiveContext.AnalysisResults["Resources"]).RawResources;
            var size = 0;
            var unmngdResTbl = _utils.MarshalResourceTable(Session.ActiveContext, out size);
            var asmName = Path.GetDirectoryName(Session.ActiveContext.DestinationAssembly) + "\\Servo.Runtime.dll";

            NativeUtils.WriteToResource(asmName, "res", unmngdResTbl, size);
            Marshal.FreeHGlobal(unmngdResTbl);
        }
    }
}
