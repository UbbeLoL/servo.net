﻿namespace Servo.Core.Context
{
    interface IContextDependent
    {
        IProtectorContext Context { get; set; }

        void Initialize(IProtectorContext context);
    }
}
