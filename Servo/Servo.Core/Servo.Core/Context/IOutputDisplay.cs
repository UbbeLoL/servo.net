﻿namespace Servo.Core.Context
{
    public enum OutputLevel : byte
    {
        Standard = 0x0,
        Verbose = 0x1
    }

    public interface IOutputDisplay
    {
        OutputLevel Level { get; set; }
        void Write(string data, OutputLevel level);
    }
}
