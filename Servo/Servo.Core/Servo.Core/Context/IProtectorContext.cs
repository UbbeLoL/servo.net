﻿using System.Collections.Generic;
using Mono.Cecil;
using Servo.Core.Analyzers.Base;
using Servo.Core.Protections.Base;

namespace Servo.Core.Context
{
    public interface IProtectorContext
    {
        Dictionary<string, IAnalysisResult> AnalysisResults { get; set; }
        AssemblyDefinition LoadedAssembly { get; set; }
        AssemblyDefinition Injections { get; set; }

        string SourceAssembly { get; set; }
        string DestinationAssembly { get; set; }

        IOutputDisplay OutputDisplay { get; set; }
        ProtectionLevel ProtectionLevel { get; set; }
    }
}
