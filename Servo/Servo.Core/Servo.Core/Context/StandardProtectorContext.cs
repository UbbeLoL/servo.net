﻿using System.Collections.Generic;
using Mono.Cecil;
using Servo.Core.Analyzers.Base;
using Servo.Core.Protections.Base;
using Servo.Injections;

namespace Servo.Core.Context
{
    public class StandardProtectorContext : IProtectorContext
    {
        public AssemblyDefinition LoadedAssembly { get; set; }
        public AssemblyDefinition Injections { get; set; }
        public string SourceAssembly { get; set; }
        public string DestinationAssembly { get; set; }
        public Dictionary<string, IAnalysisResult> AnalysisResults { get; set; }
        public IOutputDisplay OutputDisplay { get; set; }
        public ProtectionLevel ProtectionLevel { get; set; }

        public StandardProtectorContext(AssemblyDefinition loadedAssembly, string sourceAssembly, string sourceDestination)
        {
            LoadedAssembly = loadedAssembly;
            SourceAssembly = sourceAssembly;
            DestinationAssembly = sourceDestination;
            Injections = AssemblyDefinition.ReadAssembly(typeof (Identifier).Assembly.Location);
        }
    }
}
