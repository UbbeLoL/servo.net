﻿using System;

namespace Servo.Core.Context
{
    class ConsoleOutputDisplay : IOutputDisplay
    {
        public OutputLevel Level { get; set; }

        public ConsoleOutputDisplay(OutputLevel level)
        {
            Level = level;
        }

        public void Write(string data, OutputLevel level)
        {
            switch (Level)
            {
                case OutputLevel.Verbose:
                    Console.WriteLine(data);
                    break;
                case OutputLevel.Standard:
                    if (level != OutputLevel.Verbose)
                        Console.WriteLine(data);
                    break;
            }
        }
    }
}
