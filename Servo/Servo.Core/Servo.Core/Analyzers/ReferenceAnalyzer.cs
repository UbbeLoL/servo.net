﻿using System.Collections.Generic;
using System.Linq;
using Mono.Cecil;
using Servo.Core.Analyzers.Base;
using Servo.Core.Context;
using Servo.Core.Utilities.Extensions;

namespace Servo.Core.Analyzers
{
    public class ReferenceAnalysisResult : IAnalysisResult
    {
        public List<AssemblyNameReference> NonCoreReferences { get; set; }
    }

    class ReferenceAnalyzer : IAnalyzer
    {
        public IProtectorContext Context { get; set; }
        public string Key { get { return "References"; } }
        public string Description { get; private set; }

        public void Initialize(IProtectorContext context)
        {
            Context = context;
        }

        public void Analyze(IProtectorContext context)
        {
            context.AnalysisResults.Add(Key,
                                        new ReferenceAnalysisResult
                                            {
                                                NonCoreReferences =
                                                    RecursiveReferenceIdentifier().ToList()
                                            });
        }

        public List<AssemblyNameReference> RecursiveReferenceIdentifier()
        {
            var output = new List<AssemblyNameReference>();
            GetReferences(Session.ActiveContext.LoadedAssembly).ForEach(output.Add);

            return output;
        }

        public IEnumerable<AssemblyNameReference> GetReferences(AssemblyDefinition asmDef)
        {
            foreach (var @ref in asmDef.MainModule.AssemblyReferences.Where(x => !x.IsCore()))
            {
                var resolved = @ref.ResolveReference();

                yield return @ref;

                if (resolved.MainModule.AssemblyReferences.Where(x => !x.IsCore()).ToArray().Length > 0)
                    foreach (var _ref in GetReferences(@ref.ResolveReference()))
                        yield return _ref;
            }
        }
    }
}
