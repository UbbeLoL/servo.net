﻿using Servo.Core.Context;

namespace Servo.Core.Analyzers.Base
{
    interface IAnalyzer : IContextDependent
    {
        string Key { get; }
        string Description { get; }
        void Analyze(IProtectorContext context);
    }
}
