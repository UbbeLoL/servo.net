﻿using Mono.Cecil;
using Servo.Core.Analyzers.Base;
using Servo.Core.Context;

namespace Servo.Core.Analyzers
{
    public class SubsystemAnalysisResult : IAnalysisResult
    {
        public ModuleKind Subsystem;
    }

    class SubsystemAnalyzer : IAnalyzer
    {
        public IProtectorContext Context { get; set; }
        public string Key { get { return "Subsystem"; } }
        public string Description { get; private set; }

        public void Initialize(IProtectorContext context)
        {
            
        }

        public void Analyze(IProtectorContext context)
        {
            Session.ActiveContext.AnalysisResults.Add(Key,
                                                      new SubsystemAnalysisResult
                                                          {
                                                              Subsystem =
                                                                  Session.ActiveContext
                                                                         .LoadedAssembly
                                                                         .MainModule.Kind
                                                          });
        }
    }
}
