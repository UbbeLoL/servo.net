﻿using System.Collections.Generic;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Servo.Core.Analyzers.Base;
using Servo.Core.Context;
using Servo.Core.Utilities.Extensions;

namespace Servo.Core.Analyzers
{
    public class StringAnalysisResult : IAnalysisResult
    {
       public class RawString
       {
           public int ID;
           public MethodDefinition SourceMethod;
           public Instruction SourceInstruction;

           public override string ToString()
           {
               return string.Format("[Analyze][Strings] {0}:{1}", SourceMethod.Name, SourceInstruction.Operand as string);
           }
       }

       public List<RawString> RawStrings { get; set; }
        
        public StringAnalysisResult()
        {
            RawStrings = new List<RawString>();
        }
    }

    public class StringAnalyzer : IAnalyzer
    {
        public IProtectorContext Context { get; set; }
        public string Key { get { return "Strings"; } }
        public string Description { get; private set; }

        public void Initialize(IProtectorContext context)
        {

        }

        public void Analyze(IProtectorContext context)
        {
            var strTable = new StringAnalysisResult();

            context.LoadedAssembly.FindMethods(x => x.Body.Instructions.GetOpCodeCount(OpCodes.Ldstr) > 0)
                   .ForEach(
                       x =>
                       x.Body.Instructions.FindInstructions(new[] {OpCodes.Ldstr})
                        .ForEach(y =>
                                     {
                                         var rawString = new StringAnalysisResult.RawString
                                                             {
                                                                 ID = strTable.RawStrings.Count,
                                                                 SourceMethod = x,
                                                                 SourceInstruction = y
                                                             };
                                         strTable.RawStrings.Add(rawString);
                                         Session.ActiveContext.OutputDisplay.Write(rawString.ToString(), OutputLevel.Verbose);
                                     }));

            context.AnalysisResults.Add(Key, strTable);
        }
    }
}
