﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mono.Cecil;
using Servo.Core.Analyzers.Base;
using Servo.Core.Context;

namespace Servo.Core.Analyzers
{
    public class ResourceAnalysisResult : IAnalysisResult
    {
        public List<Tuple<Resource, int>> RawResources { get; set; }

        public ResourceAnalysisResult()
        {
            RawResources = new List<Tuple<Resource, int>>();
        }
    }

    public class ResourceAnalyzer : IAnalyzer
    {
        public IProtectorContext Context { get; set; }
        public string Key { get { return "Resources"; } }
        public string Description { get { return "Resources"; } }

        public void Initialize(IProtectorContext context)
        {
            
        }

        public void Analyze(IProtectorContext context)
        {
            var resTbl = new ResourceAnalysisResult();
            var idx = 0;

            foreach (var res in context.LoadedAssembly.MainModule.Resources)
                resTbl.RawResources.Add(Tuple.Create(res, idx++));

            context.AnalysisResults.Add(Key, resTbl);
        }
    }
}
