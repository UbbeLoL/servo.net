// Servo.Runtime.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <stdlib.h>  
#include <stdio.h>

struct String {
	int mdTok;
	int id;
	int size;
	char* buffer;
};

String* LocateString(int);
char* ReadResource(int &resSize, WCHAR* resName);
void XOR(char* buff, int size);

extern "C"
{
	__declspec(dllexport) String* __stdcall DecryptString(int nId, int mdTok)
	{
		String* buff = LocateString(nId);

		if(buff->mdTok == mdTok && buff->id == nId)
			return buff;
		else
		{
			return NULL;
		}
	}

	__declspec(dllexport) void __stdcall Free(char* buff)
	{
		free(buff);
	}
}

String* LocateString(int nId)
{
	int size = 0;
	char* resData = ReadResource(size, L"str");
	char* resBuffer = (char*)malloc(size);
	memcpy(resBuffer, resData, size);

	String *buffer = (String*)malloc(sizeof(String));

	buffer->mdTok = *(int*)(resBuffer + (sizeof(String)*nId));
	buffer->id = *(int*)(resBuffer + (sizeof(String)*nId) +4);
	buffer->size = *(int*)(resBuffer + (sizeof(String)*nId) +8);
	buffer->buffer = resBuffer + *(int*)(resBuffer + (sizeof(String)*nId) +12);

	return buffer;
}

char* ReadResource(int &resSize, WCHAR* resName)
{
	 HMODULE hLibrary;
	 HRSRC hResource;
	 HGLOBAL hResourceLoaded;
	 LPBYTE lpBuffer;

	 char lpFilename[MAX_PATH] = "";
	 GetModuleFileNameA(NULL, lpFilename, MAX_PATH);

	 hLibrary = LoadLibrary(L"C:\\Users\\Mattias\\Documents\\Visual Studio 2012\\Projects\\ILDbg\\TestApp\\bin\\Debug\\Servo.Runtime.dll");
	 if (NULL != hLibrary)
	 {
 hResource = FindResource(hLibrary, resName, MAKEINTRESOURCE(RT_RCDATA));
		 if (NULL != hResource)
		 {
			 hResourceLoaded = LoadResource(hLibrary, hResource);
			 if (NULL != hResourceLoaded)        
			 {
				 lpBuffer = (LPBYTE) LockResource(hResourceLoaded);            
				 if (NULL != lpBuffer)            
				 { 
					resSize = SizeofResource(hLibrary, hResource);
                    return (char*)lpBuffer;           
				 }
			 }
		 FreeLibrary(hLibrary);
	 }
}
}