﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Servo.Core;

namespace Servo.Test
{

    class Program
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct UnmanagedString
        {
            public int mdTok;
            public int Id;
            public int Size;
            public IntPtr Buffer;
        }

        [DllImport("Servo.Runtime.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr DecryptString(int nId, int mdTok);

        [DllImport("Servo.Runtime.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern void Free(IntPtr buffer);

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static int GetToken()
        {
            var st = new StackTrace();
            var sf = st.GetFrame(1);

            return sf.GetMethod().MetadataToken;
        }

        public static string FetchString(int nId, int mdTok)
        {
            var ptr = DecryptString(nId, mdTok);

            if (ptr == IntPtr.Zero)
                return null;

            var unmngdStr = (UnmanagedString)Marshal.PtrToStructure(ptr, typeof(UnmanagedString));
            var mngdStr = Marshal.PtrToStringUni(unmngdStr.Buffer);

            Free(ptr);

            return mngdStr;
        }

        static void Main(string[] args)
        {
            var prot = new Protector();

            prot.ProcessFile(
                @"C:\Users\Mattias\Documents\Visual Studio 2012\Projects\ILDbg\TestApp\bin\Debug\TestApp.exe");

            Console.ReadLine();

            var a = FetchString(1, 06000002);
      
            Console.WriteLine(a);
            Console.ReadLine();
        }
    }
}
