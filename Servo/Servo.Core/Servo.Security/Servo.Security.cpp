// Servo.Security.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"

//void WriteStringTable(char** resData, int size);

extern "C"
{
	__declspec(dllexport) void __stdcall XOR(char* buff, int size)
	{
		for (int i=0;i<size;i++)
			buff[i] ^= 5;
	}

	__declspec(dllexport) void __stdcall WriteToResource(char* name, WCHAR* resName, char** resData, int size)
	{
		 HANDLE hResource;

		hResource = BeginUpdateResourceA(name, FALSE);
		MessageBoxA(NULL, name, "", MB_OK);

		if (NULL != hResource)
			{
				//char* id = "xxx";

				//switch(*resData[0])
				//{
				//case 0:
				//	id = "str";
				//case 1:
				//	id = "res";
				//case 2:
				//	id = "unk";
				//}

			if (UpdateResource(hResource, MAKEINTRESOURCE(RT_RCDATA), resName, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), 
				(LPVOID)resData, size) != FALSE)
				{
					MessageBoxA(NULL, "Yep", "", MB_OK);
					EndUpdateResource(hResource, FALSE);
				}
			else
				MessageBoxA(NULL, "[ERROR] Internal error; UpdateResource failed!", "", MB_OK);
			}
		else
			MessageBoxA(NULL, "[ERROR] Internal error; hResource is null!", "", MB_OK);
	}
}
