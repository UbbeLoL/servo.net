﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servo.Injections
{
    public static class Initializor
    {
        public static void Initialize()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (o, e) => ResourceWorker.FetchResource(e);
        }
    }
}
