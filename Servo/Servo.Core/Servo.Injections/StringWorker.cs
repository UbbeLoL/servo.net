﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Servo.Injections
{
    public static class StringWorker
    {
        public sealed class HC128
        {
            // state
            private UInt32[] _P;
            private UInt32[] _Q;
            private byte[] _key;
            private byte[] _iv;
            private UInt32 _count = 0;

            /// <summary>
            /// Initialize a new instance of HC-128 stream cipher using specified parameters
            /// </summary>
            /// <param name="key">Key (128 bits)</param>
            /// <param name="iv">IV (128 bits)</param>
            public HC128(byte[] key, byte[] iv)
            {
                _key = key;
                _iv = iv;
            }

            public byte[] EncryptDecrypt(byte[] data)
            {
                InitState();
                byte[] keyStream = new byte[data.Length + (data.Length % 4)];

                for (int i = 0; i < keyStream.Length / 4; i++)
                {
                    byte[] keyB = BitConverter.GetBytes(GenerateKeyData());
                    Buffer.BlockCopy(keyB, 0, keyStream, i * 4, 4);
                }

                byte[] result = new byte[data.Length];

                for (int i = 0; i < data.Length; i++)
                {
                    result[i] = (byte)(data[i] ^ keyStream[i]);
                }

                return result;
            }

            /// <summary>
            /// Initialize cipher state
            /// </summary>
            private void InitState()
            {
                if (_key.Length != 16)
                    throw new CryptographicException("invalid key size");

                if (_iv.Length != 16)
                    throw new CryptographicException("invalid IV size");

                _count = 0;

                _P = new uint[512];
                _Q = new uint[512];
                UInt32[] KP = new uint[4];
                UInt32[] IVP = new uint[4];
                UInt32[] W = new uint[1280];

                KP[0] = BitConverter.ToUInt32(_key, 0);
                KP[1] = BitConverter.ToUInt32(_key, 4);
                KP[2] = BitConverter.ToUInt32(_key, 8);
                KP[3] = BitConverter.ToUInt32(_key, 12);

                IVP[0] = BitConverter.ToUInt32(_iv, 0);
                IVP[1] = BitConverter.ToUInt32(_iv, 4);
                IVP[2] = BitConverter.ToUInt32(_iv, 8);
                IVP[3] = BitConverter.ToUInt32(_iv, 12);

                for (int i = 0; i < 8; i++)
                    W[i] = KP[i % 4];

                for (int i = 0; i < 8; i++)
                    W[i + 8] = IVP[i % 4];

                for (int i = 16; i < 1280; i++)
                    W[i] = (uint)(F2(W[i - 2]) + W[i - 7] + F1(W[i - 15]) + W[i - 16] + i);

                for (int i = 0; i < 512; i++)
                    _P[i] = W[i + 256];

                for (int i = 0; i < 512; i++)
                    _Q[i] = W[i + 768];

                for (int i = 0; i < 512; i++)
                    _P[i] = (_P[i] +
                   G1(_P[MinMod512((uint)i, 3)], _P[MinMod512((uint)i, 10)], _P[MinMod512((uint)i, 511)]))
                    ^ H1(_P[MinMod512((uint)i, 12)]);

                for (int i = 0; i < 512; i++)
                    _Q[i] = (_Q[i] +
                   G2(_Q[MinMod512((uint)i, 3)], _Q[MinMod512((uint)i, 10)], _Q[MinMod512((uint)i, 511)]))
                    ^ H2(_Q[MinMod512((uint)i, 12)]);

            }

            /// <summary>
            /// Generate a key byte
            /// </summary>
            /// <returns>Generated dword</returns>
            private UInt32 GenerateKeyData()
            {
                uint j = _count % 512;
                uint result = 0;
                if ((_count % 1024) < 512)
                {
                    _P[j] = _P[j] +
                    G1(_P[MinMod512(j, 3)], _P[MinMod512(j, 10)], _P[MinMod512(j, 511)]);

                    result = H1(_P[MinMod512(j, 12)]) ^ _P[j];
                }
                else
                {
                    _Q[j] = _Q[j] +
                    G2(_Q[MinMod512(j, 3)], _Q[MinMod512(j, 10)], _Q[MinMod512(j, 511)]);

                    result = H2(_Q[MinMod512(j, 12)]) ^ _Q[j];
                }
                _count++;
                return result;
            }

            /// <summary>
            /// a-b mod 512
            /// </summary>
            private UInt32 MinMod512(UInt32 a, UInt32 b)
            {
                return (a - b) % 512;
            }

            private UInt32 F1(UInt32 x)
            {
                return RollRight(x, 7) ^ RollRight(x, 18) ^ (x >> 3);
            }

            private UInt32 F2(UInt32 x)
            {
                return RollRight(x, 17) ^ RollRight(x, 19) ^ (x >> 10);
            }

            private UInt32 G1(UInt32 x, UInt32 y, UInt32 z)
            {
                return (RollRight(x, 10) ^ RollRight(z, 23)) + RollRight(y, 8);
            }

            private UInt32 G2(UInt32 x, UInt32 y, UInt32 z)
            {
                return (RollLeft(x, 10) ^ RollLeft(z, 23)) + RollLeft(y, 8);
            }

            private UInt32 H1(UInt32 x)
            {
                return _Q[(byte)x] + _Q[256 + (byte)(x >> 16)];
            }

            private UInt32 H2(UInt32 x)
            {
                return _P[(byte)x] + _P[256 + (byte)(x >> 16)];
            }

            private UInt32 RollRight(UInt32 x, int n)
            {
                return ((x >> n) ^ (x << (32 - n)));
            }

            private UInt32 RollLeft(UInt32 x, int n)
            {
                return ((x << n) ^ (x >> (32 - n)));
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct UnmanagedString
        {
            public int mdTok;
            public int Id;
            public int Size;
            public IntPtr Buffer;
        }

        [DllImport("Servo.Runtime.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr DecryptString(int nId, int mdTok);

        [DllImport("Servo.Runtime.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern void Free(IntPtr buffer);

        public static Dictionary<int, string> StringTable = new Dictionary<int, string>();

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static int GetToken()
        {
            var st = new StackTrace();
            var sf = st.GetFrame(2);

            return sf.GetMethod().MetadataToken;
        }

        public static string FetchString(int nId)
        {
            var mdTok = GetToken();
            var ptr = DecryptString(nId, mdTok);

            if (ptr == IntPtr.Zero)
                throw new Exception("string ptr is null");

            var unmngdStr = (UnmanagedString)Marshal.PtrToStructure(ptr, typeof(UnmanagedString));
            var encString = new byte[unmngdStr.Size];

            Marshal.Copy(unmngdStr.Buffer, encString, 0, unmngdStr.Size);

            var storedTokenData = BitConverter.GetBytes(unmngdStr.mdTok + nId);
            var callerTokenData = BitConverter.GetBytes(mdTok + nId);

            #region hashing
            Int32 dataLength = storedTokenData.Length;
            UInt32 hash = Convert.ToUInt32(dataLength);
            Int32 remainingBytes = dataLength & 3; // mod 4
            Int32 numberOfLoops = dataLength >> 2; // div 4
            Int32 currentIndex = 0;

            while (numberOfLoops > 0)
            {
                hash += BitConverter.ToUInt16(storedTokenData, currentIndex);
                UInt32 tmp = (UInt32)(BitConverter.ToUInt16(storedTokenData, currentIndex + 2) << 11) ^ hash;
                hash = (hash << 16) ^ tmp;
                hash += hash >> 11;
                currentIndex += 4;
                numberOfLoops--;
            }

            switch (remainingBytes)
            {
                case 3: hash += BitConverter.ToUInt16(storedTokenData, currentIndex);
                    hash ^= hash << 16;
                    hash ^= ((UInt32)storedTokenData[currentIndex + 2]) << 18;
                    hash += hash >> 11;
                    break;
                case 2: hash += BitConverter.ToUInt16(storedTokenData, currentIndex);
                    hash ^= hash << 11;
                    hash += hash >> 17;
                    break;
                case 1: hash += storedTokenData[currentIndex];
                    hash ^= hash << 10;
                    hash += hash >> 1;
                    break;
            }

            /* Force "avalanching" of final 127 bits */
            hash ^= hash << 3;
            hash += hash >> 5;
            hash ^= hash << 4;
            hash += hash >> 17;
            hash ^= hash << 25;
            hash += hash >> 6;

            var hash1 = hash;

            //if (StringTable.ContainsKey(((int) hash1)))
            //{
            //    Console.WriteLine("I was found");
            //    return StringTable[(int) hash1 + nId];
            //}
            dataLength = callerTokenData.Length;
            hash = Convert.ToUInt32(dataLength);
            remainingBytes = dataLength & 3; // mod 4
            numberOfLoops = dataLength >> 2; // div 4
            currentIndex = 0;

            while (numberOfLoops > 0)
            {
                hash += BitConverter.ToUInt16(callerTokenData, currentIndex);
                UInt32 tmp = (UInt32)(BitConverter.ToUInt16(callerTokenData, currentIndex + 2) << 11) ^ hash;
                hash = (hash << 16) ^ tmp;
                hash += hash >> 11;
                currentIndex += 4;
                numberOfLoops--;
            }

            switch (remainingBytes)
            {
                case 3: hash += BitConverter.ToUInt16(callerTokenData, currentIndex);
                    hash ^= hash << 16;
                    hash ^= ((UInt32)callerTokenData[currentIndex + 2]) << 18;
                    hash += hash >> 11;
                    break;
                case 2: hash += BitConverter.ToUInt16(callerTokenData, currentIndex);
                    hash ^= hash << 11;
                    hash += hash >> 17;
                    break;
                case 1: hash += callerTokenData[currentIndex];
                    hash ^= hash << 10;
                    hash += hash >> 1;
                    break;
            }

            /* Force "avalanching" of final 127 bits */
            hash ^= hash << 3;
            hash += hash >> 5;
            hash ^= hash << 4;
            hash += hash >> 17;
            hash ^= hash << 25;
            hash += hash >> 6;

            var hash2 = hash;
            #endregion

            Free(ptr);

            if (hash1 == hash2)
            {
                var key = new byte[16];
                var iv = new byte[16];

                // Create Key
                Buffer.BlockCopy(BitConverter.GetBytes((mdTok + nId)), 0, key, 0, 4);
                Buffer.BlockCopy(BitConverter.GetBytes(-(mdTok + nId)), 0, key, 4, 4);
                Buffer.BlockCopy(BitConverter.GetBytes((mdTok + nId)), 0, key, 8, 4);
                Buffer.BlockCopy(BitConverter.GetBytes(-(mdTok + nId)), 0, key, 12, 4);

                // Create IV
                Buffer.BlockCopy(BitConverter.GetBytes((mdTok - nId)), 0, iv, 0, 4);
                Buffer.BlockCopy(BitConverter.GetBytes(-(mdTok - nId)), 0, iv, 4, 4);
                Buffer.BlockCopy(BitConverter.GetBytes((mdTok - nId)), 0, iv, 8, 4);
                Buffer.BlockCopy(BitConverter.GetBytes(-(mdTok - nId)), 0, iv, 12, 4);

                var hc = new HC128(key, iv);

                return Encoding.Unicode.GetString(hc.EncryptDecrypt(encString));

                //StringTable.Add(((int) hash1),
                //                Encoding.Unicode.GetString(hc.EncryptDecrypt(encString)));
                //return StringTable[((int)hash1) + nId];
            }

            throw new Exception("string hash mismatch");
        }
    }
}
