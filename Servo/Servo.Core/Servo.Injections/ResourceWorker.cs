﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Servo.Injections
{
    public static class ResourceWorker
    {
        [DllImport("Servo.Runtime.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr DecryptResource(string name, [Out] int size);

        public static Assembly FetchResource(ResolveEventArgs e)
        {
            var size = 0;
            var res = DecryptResource(e.Name, size);

            if (res == IntPtr.Zero || size == -1)
                return null;

            var buff = new byte[size];
            Marshal.Copy(res, buff, 0, size);

            return Assembly.Load(buff);
        }
    }
}
